﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalReactions : MonoBehaviour {
    public bool isStart = true;
    private TimeControler timeControler;
    private CutScene cutScene;

    public AudioClip startSound;
    public AudioClip endSound;
    private bool started = false;

    // Use this for initialization
    void Start () {
        timeControler = GameObject.FindGameObjectWithTag("GameController").GetComponent<TimeControler>();
        cutScene = GameObject.FindGameObjectWithTag("GameController").GetComponentInChildren<CutScene>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            if (isStart)
            {
                timeControler.startTimer();
                if(startSound != null && !started)
                {
                    SoundController.instance.PlaySingle(startSound);
                    started = true;
                }
            }
            else
            {
                timeControler.stopTimer();
                if(endSound != null)
                {
                    SoundController.instance.PlaySingle(endSound);
                }

                //wait untill grounded
                CharacterController cControl = col.GetComponent<CharacterController>();
                StartCoroutine(RunCutScene(cControl));
                //start end level dance
                //cutScene.PlayCutScene();
            }
            
        }
    }

    private IEnumerator RunCutScene(CharacterController cControl)
    {
        while (!cControl.isGrounded)
        {
            yield return new WaitForEndOfFrame();
        }
        cutScene.PlayCutScene();
    }
}
