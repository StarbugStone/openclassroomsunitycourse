﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucketTrap : MonoBehaviour, TriggerableObject
{
    private Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void runTrigger()
    {
        anim.Play("Bucket");
    }
}
