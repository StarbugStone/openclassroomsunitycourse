﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringControler : MonoBehaviour {

    private Animator SpringAnim;
    private AudioSource Boing;
    private Vector3 springForward;

    public float force = 100F;
    public AudioClip boingSound;

	// Use this for initialization
	void Start () {
        SpringAnim = GetComponent<Animator>();
        //Boing = GetComponent<AudioSource>();
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            //Getting the alignment of the spring in world coords
            springForward = transform.TransformDirection(new Vector3(0, 1, 0));
            //Animating and playing sounds
            SpringAnim.SetTrigger("Contact");
            SoundController.instance.PlaySingle(boingSound);
            //Boing.Play();
            //grabbing the react script and passing direction and force
            col.GetComponent<playerReactions>().throwBack(springForward, force);
        }

    }
}
