﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonBall : MonoBehaviour
{

    private GameObject gameControler;
    private SlowMotion slowmotion; //Needed to call our slow motion script. It should always be on the game controler

    private PlayerHealth playerHealth;
    public int damage = 34;

    private Vector3 colisionPoint;
    private Rigidbody canonBallRB;

    public float force = 50f;
    public GameObject explosion;
    private GameObject explosionHolder;
    public float explosionForce = 50f;
    public float explosionRadius = 10f;
    public float explosionUpForce = 1f;

    public AudioClip electrocuteSound;
    public AudioClip playerScream;

    private ParticleSystem lightningParticles; //Particles when electrocuted, this is a child gameobject on the player
    private SuperBlur.SuperBlur superBlur; //blur when electrocuted, this should be attached to the camera
    private CameraShake cameraShake; //cameraShake when electrocuted

    private bool destroyingCanonBall = false;

	// Use this for initialization
	void Start () {
        gameControler = GameObject.FindGameObjectWithTag("GameController");
        slowmotion = gameControler.GetComponent<SlowMotion>();
        canonBallRB = gameObject.GetComponent<Rigidbody>();

    }
	
	// Update is called once per frame
	void Update () {

        //if the canonball is being destroyed, initiate the 1 second shrink
		if (destroyingCanonBall)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(0, 0, 0), Time.deltaTime);
            //we might be able to get a shader working to make the ball slowly transparant.
        }
	}

    private void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Player")
        {
            //Debug.Log("collision with player");
            canonBallRB.isKinematic = true;

            canonBallRB.velocity = Vector3.zero; //the ball is exploding. Stop it moving

            lightningParticles = col.transform.Find("LightningParticles").GetComponent<ParticleSystem>();
            lightningParticles.Emit(10);

            cameraShake = col.GetComponentInChildren<CameraShake>();


            slowmotion.runSlowMotion();

            //This gets the diffrence between the canonball and the contact point. giving us the direction to shoot the player towards
            colisionPoint = col.transform.position - transform.position;
            StartCoroutine(cameraShake.ShakePosition());
            StartCoroutine(lateReaction(col, canonBallRB, damage, playerScream));

            //finished
            return;

        }

    }

    private IEnumerator lateReaction(Collider col, Rigidbody canonBallRB, int damage, AudioClip playerScream)
    {
        //Play our audio
        if(electrocuteSound != null)
        {
            SoundController.instance.PlaySingle(electrocuteSound);
        }
        

        //blur the camera
        superBlur = col.GetComponentInChildren<SuperBlur.SuperBlur>();
        superBlur.enabled = true;

        yield return new WaitForSecondsRealtime(0.7f);
        if (playerScream != null)
        {
            //Debug.Log("scream");
            SoundController.instance.PlaySingle(playerScream);
        }
        col.GetComponent<playerReactions>().throwBack(colisionPoint, force);
        //play explosion
        //canonBallRB.AddExplosionForce(explosionForce, gameObject.transform.position, explosionRadius);
        //doesn't apear to work. I think this actualy applys a force to a specific gameObject, Not create an explosion force to all the game objects around

        playerHealth = col.GetComponent<PlayerHealth>();
        playerHealth.looseHealth(damage);

        superBlur.enabled = false;
        Destroy(gameObject);
        explosionHolder = Instantiate(explosion, transform.position, transform.rotation);
        Destroy(explosionHolder, 1f);
        
    }

    //this is called from the canon.
    public IEnumerator destroyBallAfterTime(float lifeTime)
    {
        Destroy(gameObject, lifeTime); //initiate the destroy in n seconds
        yield return new WaitForSeconds(lifeTime-1); //at n-1 initiate the shrink animation
        destroyingCanonBall = true;
            
    }



}
