﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovementType
{
    moveUp, moveRight, moveForward
}

public class MovablePlatform : MonoBehaviour {

    public MovementType movement;
    [Range(0.0f, 1.0f)]
    public float startOffset = 0f;
    
	// Use this for initialization
	void Start () {
        Animator anim = GetComponent<Animator>();
        //AnimatorStateInfo state = anim.GetCurrentAnimatorStateInfo(0);//could replace 0 by any other animation layer index
        //anim.Play(state.fullPathHash, -1, Random.Range(0f, 1f));


        if (movement == MovementType.moveRight)
        {
            anim.Play("MovablePlatformRight", -1, startOffset);
        }
        if (movement == MovementType.moveUp)
        {
            anim.Play("MovablePlatformUp",-1, startOffset);
        }
        if (movement == MovementType.moveForward)
        {
            anim.Play("MovablePlatformForward", -1, startOffset);
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay(Collider col)
    {
        //Debug.Log("Any collision");
        if (col.tag == "Player")
        {
            //Debug.Log("collision");
            //If the platform hits the player
            col.transform.parent = transform;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            //Debug.Log("Leaving collision");
            //If the platform hits the player
            col.transform.parent = null;
        }
    }
}
