﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableWall : MonoBehaviour, TriggerableObject
{

    public GameObject DestructableWallGo;
    public GameObject DestructableWallGoDestroyed;
    public BoxCollider DestructableWallBoxCollider;
    public GameObject explosion;
    public Transform testExp;

    public float power = 3000f;
    public float radius = 5f;
    

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "CanonBall")
        {
            col.GetComponent<Rigidbody>().velocity = Vector3.zero;
            DestroyTheWall();
            
            StartCoroutine(col.GetComponent<CanonBall>().destroyBallAfterTime(0.4f));

        }
    }

    public void runTrigger()
    {
        DestroyTheWall();
    }

    private void DestroyTheWall()
    {
        
        // Change all our 3D elements to destroyed ones
        DestructableWallGo.SetActive(false);
        DestructableWallGoDestroyed.SetActive(true);
        DestructableWallBoxCollider.enabled = false;

        //Add the explosion particle effect
        explosion.transform.position = Vector3.zero;
        GameObject partEffect = Instantiate(explosion, testExp);
        Destroy(partEffect, 0.9f);

        //DESTROY !!!! EXPLODE !!!! 
        Rigidbody[] parts = DestructableWallGoDestroyed.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody part in parts)
        {

            //Debug.Log(part.name);
            part.AddExplosionForce(power, testExp.position, radius);
        }
    }
}
