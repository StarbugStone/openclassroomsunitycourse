﻿public interface TriggerableObject {

    void runTrigger();
}
