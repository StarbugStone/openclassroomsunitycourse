﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;

public class ControlCanon : MonoBehaviour {

    public GameObject controlableCanon;
    public float rotationSpeed = 30f;
    public float timeBetweenShots = 0.8f;
    private float lastShotTime;

    public bool canControl = false;
    //private bool isControling = false;


    private GameObject player;
    private FirstPersonController fpc;

    private Camera MainCamera;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        fpc = player.GetComponent<FirstPersonController>();
        MainCamera = Camera.main;
        lastShotTime = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
        if (CrossPlatformInputManager.GetButtonDown("Use") && canControl)
        {
            //isControling = !isControling; //toggle state
            if (!GameState.instance.controlingCanon)
            {
                GameState.instance.controlingCanon = true;
                ControlCanonStart();
            }
            else
            {
                GameState.instance.controlingCanon = false;
                ControlCanonEnd();
            }
        }

        if (CrossPlatformInputManager.GetButtonDown("Fire1") && GameState.instance.controlingCanon)
        {
            FireCanon();
        }

        if(CrossPlatformInputManager.GetAxis("Horizontal") > 0 && GameState.instance.controlingCanon)
        {
            controlableCanon.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
        }

        if(CrossPlatformInputManager.GetAxis("Horizontal") < 0 && GameState.instance.controlingCanon)
        {
            controlableCanon.transform.Rotate(Vector3.back * Time.deltaTime * rotationSpeed);
        }

    }

    private void ControlCanonStart()
    {
        fpc.enabled = false;
        MainCamera.transform.LookAt(controlableCanon.transform);
        GameState.instance.controlingCanon = true;
    }

    private void ControlCanonEnd()
    {
        fpc.enabled = true;
        GameState.instance.controlingCanon = false;
    }

    private void FireCanon()
    {
        if (!GameState.instance.isPaused)
        {
            if(Time.time - lastShotTime > timeBetweenShots)
            {
                controlableCanon.GetComponent<Canon>().shoot();
                lastShotTime = Time.time;
            }
            
            //TODO : cooldown on shot
        }

    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            canControl = true;
            col.GetComponent<PlayerHealth>().controlingObject = gameObject;
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            canControl = false;
            col.GetComponent<PlayerHealth>().controlingObject = null;
            GameState.instance.controlingCanon = false;
            ControlCanonEnd();
        }
    }



    void OnDrawGizmosSelected()
    {
        if(controlableCanon != null)
        {
            Gizmos.DrawLine(transform.position, controlableCanon.transform.position);
        }
    }
}
