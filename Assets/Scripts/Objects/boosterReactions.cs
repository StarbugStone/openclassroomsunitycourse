﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boosterReactions : MonoBehaviour {

    //Texture animation
    public float scrollSpeed = 0.5F;
    private Renderer rend;
    //private float initialScroolSpeed;

    //force application
    public float force = 100F;
    private Vector3 boostForward;

    public AudioClip wooshSound;

    // Use this for initialization
    void Start () {
        //Texture animation
        rend = GetComponent<Renderer>();
    }
	

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            boostForward = transform.TransformDirection(new Vector3(1, 0, 0));
            //grabbing the react script and passing direction and force
            col.GetComponent<playerReactions>().throwBack(boostForward, force);
            SoundController.instance.PlaySingle(wooshSound);
        }
    }

    void Update()
    {
        //Texture animation
        float offset = Time.time * scrollSpeed;
        rend.material.mainTextureOffset = new Vector2(offset, 0);
    }
}
