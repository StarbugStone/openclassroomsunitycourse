﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerObjects : MonoBehaviour {

    public List<GameObject> triggerObjects;
    public AudioClip clickSound;


    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            SoundController.instance.PlaySingle(clickSound);
            foreach (GameObject trigger in triggerObjects)
            {
                if(trigger != null) //check if the object hasn't been destroyed
                {
                    //could add a paticle flash on each object to visualy show the awake state
                    TriggerableObject scriptToTrigger = trigger.GetComponent<TriggerableObject>();
                    scriptToTrigger.runTrigger();
                }
                

            }
        }
    }

    //add some lines to the creation screen for ease of dev
    void OnDrawGizmosSelected()
    {
        if(triggerObjects.Count > 0) //no need to draw lines until we have an actual list
        {
            Gizmos.color = Color.green;
            foreach (GameObject trigger in triggerObjects)
            {
                if(trigger != null)
                {
                    Gizmos.DrawLine(transform.position, trigger.transform.position);
                }
                
            }
        }
        
    }
}
