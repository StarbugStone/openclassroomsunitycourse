﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon : MonoBehaviour, TriggerableObject {

    [Header("Instatiate Cannon Ball")]
    public Transform firePoint;
    public GameObject canonBall;
    private Animator Anim;
    private ParticleSystem particles;
    public float speed = 50f;
    public float canonBallLifetime = 10f;
    private AudioSource canonAudio;
    private GameObject firedBall;

    [Space]
    [Header("Options for trigger")]
    public float fireRate = 1f;
    public int numberOfShots = 4;
    private bool isShooting = false;
    public bool randomStartTime = false;

    // Use this for initialization
    void Start () {
        Anim = GetComponent<Animator>();
        particles = GetComponentInChildren<ParticleSystem>(); //smoke particle system
        canonAudio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		/*if (Input.GetKeyDown("f"))
        {
            //Debug.Log("fire");
            shoot();
        }*/
	}

    public void shoot()
    {
        canonAudio.Play();
        Anim.SetTrigger("FireCanon");
        firedBall = Instantiate(canonBall, firePoint.position, firePoint.rotation);
        //Destroy(firedBall, canonBallLifetime); //prehaps a better effect of the canonball fading out and shrinking ? Create a public function on the canonBall and call it from here
        StartCoroutine(firedBall.GetComponent<CanonBall>().destroyBallAfterTime(canonBallLifetime)); //yes this works better
        firedBall.GetComponent<Rigidbody>().velocity = firePoint.transform.forward * speed;
        particles.Emit(20);
    }

    private IEnumerator fireSequance(float fireRate, int numberOfShots)
    {
        isShooting = true;
        while (numberOfShots > 0)
        {
            if (randomStartTime)
            {
                yield return new WaitForSeconds(Random.Range(0f, 0.3f));
            }
            shoot();
            yield return new WaitForSeconds(fireRate);
            numberOfShots -= 1;
        }
        
        isShooting = false;
    }

    public void runTrigger()
    {
        //Debug.Log("CanonTriggered");
        if (!isShooting)
        {
            StartCoroutine (fireSequance(fireRate, numberOfShots));
        }
    }


}
