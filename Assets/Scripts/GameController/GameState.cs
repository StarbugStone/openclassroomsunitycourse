﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour {

    public static GameState instance = null;

    public bool isPaused = false;
    public bool playingCutScene = false;
    public bool endLevel = false;
    public bool isStartMenu = false;
    public bool controlingCanon = false;

    private void Awake()
    {
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);
    }

}
