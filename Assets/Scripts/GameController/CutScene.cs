﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityStandardAssets.Characters.FirstPerson;

public class CutScene : MonoBehaviour {

    private PlayableDirector cutSceneAnim;
    //private PauseMenu pauseMenu;

    public GameObject FpsCharacter;
    public GameObject cutSceneCamera;
    public FirstPersonController fpc;
    public GameObject FpsCharacterModel;
    private Vector3 originalPosition;

    public string nextLevelName = "end";


	// Use this for initialization
	void Start () {
        cutSceneAnim = GetComponent<PlayableDirector>();
        //pauseMenu = GameObject.FindGameObjectWithTag("GameController").GetComponent<PauseMenu>();
        originalPosition = FpsCharacterModel.transform.localPosition;
        

    }
	
	// Update is called once per frame
	void Update () {
        if (GameState.instance.playingCutScene && cutSceneAnim.state != PlayState.Playing && !GameState.instance.isPaused)
        {
            //The cut scene has finished and we are not paused
            GameState.instance.playingCutScene = false;
            FpsCharacterModel.transform.localPosition = originalPosition;
            fpc.enabled = true;
            FpsCharacter.SetActive(true);
            cutSceneCamera.SetActive(false);
            GameState.instance.endLevel = true;
            
            //Go to next level
            LevelLoader nextLevel = GameObject.FindGameObjectWithTag("GameController").GetComponent<LevelLoader>();
            if (nextLevelName == "end")
            {
                nextLevel.LoadScene("_StartMenu");
            }
            else
            {
                nextLevel.LoadScene(nextLevelName);
            }
            
            
        }
    }

    public void PlayCutScene()
    {
        //stop music
        SoundController.instance.StopMusic();
        //disable First Person Controller Script
        fpc.enabled = false;
        FpsCharacter.SetActive(false);
        cutSceneCamera.SetActive(true);
        GameState.instance.playingCutScene = true;
        cutSceneAnim.Play();
        //How do we play the cutscene sound via the Audio Mixer ?????
        
    }
}
