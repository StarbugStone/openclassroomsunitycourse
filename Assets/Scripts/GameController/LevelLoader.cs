﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

    public void LoadScene(string sceneName)
    {
        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    
}
