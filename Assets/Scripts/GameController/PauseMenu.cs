﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    private GameObject pauseMenu;
    private Slider musicVolumeSlider;
    private Slider fxVolumeSlider;
    private GameObject player;

   

    private FirstPersonController fpc;
    private float lastTimeScale = 1;

	// Use this for initialization
	void Start () {
        pauseMenu = GameObject.FindGameObjectWithTag("PauseMenu");
        if(pauseMenu == null)
        {
            Debug.LogError("No Pause menu, is the Pause Menu Prefab in the scene ?");
        }
        musicVolumeSlider = GameObject.FindGameObjectWithTag("MusicVolume").GetComponent<Slider>();
        fxVolumeSlider = GameObject.FindGameObjectWithTag("FXVolume").GetComponent<Slider>();
        player = GameObject.FindGameObjectWithTag("Player");

        pauseMenu.SetActive(false);
        fpc = player.GetComponent<FirstPersonController>();

        //Getting the player prefabs for the music volumes and setting the sliders to the right value
        float[] setVolumes = SoundController.instance.GetVolumePrefabs();
        musicVolumeSlider.value = setVolumes[0];
        fxVolumeSlider.value = setVolumes[1];
    }
	
	// Update is called once per frame
	void Update () {
        if (CrossPlatformInputManager.GetButtonDown("Pause"))
        {
            PauseGame();
        }

    }

    public void PauseGame()
    {
        if (pauseMenu.activeSelf == false)
        {
            //activate menu and pause the game
            pauseMenu.SetActive(true);
            GameState.instance.isPaused = true;
            fpc.enabled = false;
            lastTimeScale = Time.timeScale;
            Time.timeScale = 0;
            SoundController.instance.PauseAllSound();

            //take care of mouse
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
        }
        else
        {
            if (!GameState.instance.endLevel)
            {
                //resume
                pauseMenu.SetActive(false);
                GameState.instance.isPaused = false;
                if (!GameState.instance.playingCutScene && !GameState.instance.controlingCanon)
                {
                    //the cut scene controller takes care of this
                    fpc.enabled = true;
                }
                Time.timeScale = lastTimeScale;
                SoundController.instance.UnPauseAllSound();

                //take care of mouse
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            
        }
    }

    public void ReloadLevel()
    {
        int scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene, LoadSceneMode.Single);

    }

    public void QuitGame()
    {
        //need to reset the timescale
        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        SceneManager.LoadScene("_StartMenu", LoadSceneMode.Single);
        //Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;
    }
}
