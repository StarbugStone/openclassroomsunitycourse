﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour {


    private Animator idleAnim;
    private string[] waitStates = { "Arm", "Dwarf", "Look", "Point", "Wave" };

    // Use this for initialization
    void Start () {
        idleAnim = GameObject.FindGameObjectWithTag("CutscenePlayer").GetComponentInChildren<Animator>();
        StartCoroutine(runRandomIdleAnim());
        //set the sliders
        Slider musicVolumeSlider = GameObject.FindGameObjectWithTag("MusicVolume").GetComponent<Slider>();
        Slider fxVolumeSlider = GameObject.FindGameObjectWithTag("FXVolume").GetComponent<Slider>();
        float[] setVolumes = SoundController.instance.GetVolumePrefabs();
        musicVolumeSlider.value = setVolumes[0];
        fxVolumeSlider.value = setVolumes[1];
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //TODO: Addthe best scores for each level on the start menu canvas


    private IEnumerator runRandomIdleAnim()
    {
        while (true)
        {
            yield return new WaitForSeconds(12f);
            idleAnim.SetTrigger(waitStates[Random.Range(0, (waitStates.Length))]);
        }
        

    }

    public void loadSceneWait(string sceneName)
    {
        //LoadSceneMode.Additive
        //SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        idleAnim.SetTrigger("Start");
        StartCoroutine(WaitForAnimAndLoadScene(sceneName));
    }

    private IEnumerator WaitForAnimAndLoadScene(string sceneName, float time = 0.5f)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

}
