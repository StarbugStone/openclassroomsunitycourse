﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportToStart : MonoBehaviour {

    private Vector3 startPosition = new Vector3(0f, 1.5f, 0f);
    private GameObject particles;
    private GameObject player;
    private PlayerHealth playerHealth;

    public AudioClip respawnSound;

    public Vector3 cheatTeleport;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        particles = player.transform.Find("RegenParticles").gameObject;
        playerHealth = player.GetComponent<PlayerHealth>();
    }
	
    void Update()
    {
        /*if (Input.GetKeyDown("t"))
        {
            Debug.Log("teleporting");
            player.transform.position = cheatTeleport;
        }*/
    }

    
    public void teleportToStart()
    {
        player.transform.position = startPosition; //we still have movement affected to the player. Will need to sort that out. Probably when the death animation comes.
        playerHealth.isDead = false;
        playerHealth.fullHealth();
        if (respawnSound != null)
        {
            SoundController.instance.PlaySingle(respawnSound); //we play our sound via the sound controller
        }
        
        if (particles != null)
        {
            particles.SetActive(true);
        }
    }
}
