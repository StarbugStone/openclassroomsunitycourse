﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeControler : MonoBehaviour {

    private Text timeText;
    private string timePassed;
    private bool isRunning = false;
    private float passedTime = 0f;

    private Text bestTimeText;
    private Text bestTimeText2;
    private float bestTime = -1;
    private string levelName;

    // Use this for initialization
    void Start () {
        timeText = GameObject.FindGameObjectWithTag("TimeText").GetComponent<Text>();
        bestTimeText = GameObject.FindGameObjectWithTag("BestTimeText").GetComponent<Text>();
        bestTimeText2 = GameObject.FindGameObjectWithTag("BestTimeText2").GetComponent<Text>();
        if(timeText == null)
        {
            Debug.LogError("TimeText not found, have you added the HudCanvas Prefab ?");
        }
        levelName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        timePassed = "00:00";
        timeText.text = "Temps : " + timePassed.ToString();
        if (PlayerPrefs.HasKey(levelName + "-BestTime"))
        {
            bestTime = PlayerPrefs.GetFloat(levelName + "-BestTime");
            ShowBestTime(bestTime);
        }
        

    }
	
	// Update is called once per frame
	void Update () {
        if (isRunning)
        {
            passedTime += Time.deltaTime;

            timePassed = Mathf.Floor(passedTime / 60).ToString("00") + ":" + Mathf.Floor(passedTime % 60).ToString("00");
            timeText.text = "Temps : " + timePassed;
        }
    }

    public void startTimer()
    {
        isRunning = true;
        
    }

    public void stopTimer()
    {
        isRunning = false;

        if (IsBestTime())
        {
            //Debug.Log("time : "+timePassed);
            bestTimeText2.text = "NOUVEAU Meilleur Temps : " + timePassed;
        }
    }

    public bool IsBestTime()
    {
        if(bestTime > 0) //set to -1 if nothing set
        {
            if(passedTime < bestTime)
            {
                PlayerPrefs.SetFloat(levelName + "-BestTime", passedTime);
                //ShowBestTime(passedTime);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            PlayerPrefs.SetFloat(levelName + "-BestTime", passedTime);
            ShowBestTime(passedTime);
            return true;
        }
    }

    private void ShowBestTime(float newBestTime)
    {
        bestTimeText.text = "Meilleur Temps : "+ Mathf.Floor(newBestTime / 60).ToString("00") + ":" + Mathf.Floor(newBestTime % 60).ToString("00");
    }

    
}
