﻿using UnityEngine;

public class SlowMotion : MonoBehaviour {

    public float slowSpeed = 0.05f;
    public float slowTime = 2f;


    private void Update()
    {
        if (!GameState.instance.isPaused)
        {
            Time.timeScale += (1f / slowTime) * Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
            Time.fixedDeltaTime = Time.timeScale * 0.02f; //resetting the fixed delta time to the norm of 0.2
        }
        
    }

    public void runSlowMotion()
    {
        Time.timeScale = slowSpeed;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }
}
