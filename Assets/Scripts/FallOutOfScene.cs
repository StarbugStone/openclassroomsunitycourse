﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallOutOfScene : MonoBehaviour {

    private TeleportToStart teleportToStart;

    // Use this for initialization
    void Start () {
        teleportToStart = GameObject.FindGameObjectWithTag("GameController").GetComponent<TeleportToStart>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            //teleport to start
            teleportToStart.teleportToStart();
        }
        else
        {
            Destroy(col.gameObject); //if it isn't the player then it is debry. We can safely remove it from the game
        }
    }
}
