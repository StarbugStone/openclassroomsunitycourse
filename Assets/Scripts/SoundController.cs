﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Audio;

public class SoundController : MonoBehaviour {

    public AudioSource efxSource;                   //Drag a reference to the audio source which will play the sound effects.
    public AudioSource musicSource;                 //Drag a reference to the audio source which will play the music.
    public AudioSource demoEfxSource;               //source for demo sound when setting volumes
    public static SoundController instance = null;  //Allows other scripts to call functions from SoundManager.             
    public float lowPitchRange = .95f;              //The lowest a sound effect will be randomly pitched.
    public float highPitchRange = 1.05f;            //The highest a sound effect will be randomly pitched.

    public AudioClip[] demoEfxSound;

    //for the prefab first person controller sounds
    //private AudioSource playerAudioSource;

    //testing out AudioMixers
    private AudioMixer musicMixer;
    private AudioMixer efxMixer;
    //public AudioMixer spacialEfxMixer;

    private void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        //DontDestroyOnLoad(gameObject);

        
    }

    private void Start()
    {
        //audioMixers ?
        musicMixer = musicSource.outputAudioMixerGroup.audioMixer;
        efxMixer = efxSource.outputAudioMixerGroup.audioMixer;

        //grabbing the playerPrefs and putting the mixer volumes to the level
        float[] setVolumes = GetVolumePrefabs();
        musicMixer.SetFloat("MusicVolume", setVolumes[0]);
        efxMixer.SetFloat("EfxVolume", setVolumes[1]);


    }

    //Used to play single sound clips.
    public void PlaySingle(AudioClip clip)
    {
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        efxSource.clip = clip;

        //Play the clip.
        efxSource.Play();
    }


    //RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
    public void RandomizeSfx(params AudioClip[] clips)
    {
        //Generate a random number between 0 and the length of our array of clips passed in.
        int randomIndex = UnityEngine.Random.Range(0, clips.Length);

        //Choose a random pitch to play back our clip at between our high and low pitch ranges.
        float randomPitch = UnityEngine.Random.Range(lowPitchRange, highPitchRange);

        //Set the pitch of the audio source to the randomly chosen pitch.
        efxSource.pitch = randomPitch;

        //Set the clip to the clip at our randomly chosen index.
        efxSource.clip = clips[randomIndex];

        //Play the clip.
        efxSource.Play();
    }

    public void StartMusic()
    {
        musicSource.Play();
    }

    public void StopMusic()
    {
        musicSource.Stop();
    }

    public void PauseAllSound()
    {
        //musicSource.Pause();
        efxSource.Pause();

    }

    public void UnPauseAllSound()
    {
        //musicSource.UnPause();
        efxSource.UnPause();
    }

    public void SetMusicVolume(Single volume)
    {
        musicMixer.SetFloat("MusicVolume", volume);
        //set as player pref
        PlayerPrefs.SetFloat("MusicVolume", volume);
    }

    public void SetFxVolume(Single volume)
    {
        //play the demo sound with random pitch
        if (!demoEfxSource.isPlaying && GameState.instance.isPaused || !demoEfxSource.isPlaying && GameState.instance.isStartMenu)
        {
            int randomIndex = UnityEngine.Random.Range(0, demoEfxSound.Length);
            float randomPitch = UnityEngine.Random.Range(lowPitchRange, highPitchRange);
            //Set the pitch of the audio source to the randomly chosen pitch.
            demoEfxSource.pitch = randomPitch;
            //Set the clip to the clip at our randomly chosen index.
            demoEfxSource.clip = demoEfxSound[randomIndex];
            //Play the clip.
            demoEfxSource.Play();
        }
        efxMixer.SetFloat("EfxVolume", volume);
        //set as player pref
        PlayerPrefs.SetFloat("FxVolume", volume);
    }

    public float[] GetVolumePrefabs()
    {
        float[] volumes = new float[2];
        volumes[0] = 0f;
        volumes[1] = 0f;
        
        //grabbing the playerPrefs
        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            volumes[0] = PlayerPrefs.GetFloat("MusicVolume");
        }
        if (PlayerPrefs.HasKey("FxVolume"))
        {
            volumes[1] = PlayerPrefs.GetFloat("FxVolume");
        }
        return volumes;
    }
}
