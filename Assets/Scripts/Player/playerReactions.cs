﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerReactions : MonoBehaviour {
    private CharacterController cControl;
    private Vector3 impact = Vector3.zero;

	// Use this for initialization
	void Start () {
        cControl = GetComponent<CharacterController>();	
	}
	
	// Update is called once per frame
	void Update () {
		//apply the impacting force
        if(impact.magnitude > 0.2)
        {
            cControl.Move(impact * Time.deltaTime);
        }
        //dimish impact over time
        impact = Vector3.Lerp(impact, Vector3.zero, 5 * Time.deltaTime);
	}

    public void throwBack(Vector3 backMotion, float force)
    {
        
        backMotion.Normalize();
        //Debug.Log("called : " + backMotion);
        impact += backMotion.normalized * force;

    }
}
