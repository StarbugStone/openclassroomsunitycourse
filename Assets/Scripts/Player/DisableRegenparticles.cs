﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableRegenparticles : MonoBehaviour {

    public float particleDuration = 3f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnEnable()
    {
        Invoke("disableObject", particleDuration);
    }

    void disableObject()
    {
        gameObject.SetActive(false);
    }
}
