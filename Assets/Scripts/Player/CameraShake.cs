﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    public float duration = 2f;
    public float speed = 20f;
    public float magnitude = 2f;

    private Vector3 originalPosition;

    // Use this for initialization
    void Start () {
        originalPosition = transform.localPosition;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public IEnumerator ShakePosition()
    {
        float elapsed = 0f;
        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            //Debug.Log(Time.time * speed);
            float x = (Mathf.PerlinNoise(Time.time * speed, 0f) * magnitude) - (magnitude / 2f);
            float y = (Mathf.PerlinNoise(0f, Time.time * speed) * magnitude) - (magnitude / 2f);
            transform.localPosition = new Vector3(originalPosition.x + x, originalPosition.y + y, originalPosition.z);
            yield return null;
        }
        transform.localPosition = originalPosition;
    }
}
