﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerHealth : MonoBehaviour {

    public int health = 100;
    private int startHealth;
    public bool isDead = false;
    private Slider healthSlider;

    private TeleportToStart teleportToStart;

    public GameObject controlingObject;
    


    // Use this for initialization
    void Start () {
        healthSlider = GameObject.FindGameObjectWithTag("HealthSlider").GetComponent<Slider>();
        if(healthSlider == null)
        {
            Debug.LogError("No healthSlider present, is the UI in the project ?");
        }

        healthSlider.maxValue = health;
        teleportToStart = GameObject.FindGameObjectWithTag("GameController").GetComponent<TeleportToStart>();
        startHealth = health;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void looseHealth(int amountLost)
    {
        health -= amountLost;
        healthSlider.value = health;
        if (health <= 0)
        {
            isDead = true; //needed to block all movement; Might just call a disable from here.
            StartCoroutine(isDeadActions());
        }

    }

    private IEnumerator isDeadActions()
    {
        if(controlingObject != null)
        {
            controlingObject.GetComponent<ControlCanon>().canControl = false;
            controlingObject = null;

        }
        
        GameState.instance.controlingCanon = false;
        GetComponent<FirstPersonController>().enabled = false;
        //GetComponent<CharacterController>().enabled = false; //stop movement
        yield return new WaitForSecondsRealtime(0.4f);
        teleportToStart.teleportToStart();
        yield return new WaitForSecondsRealtime(0.7f);
        //GetComponent<CharacterController>().enabled = true;
        if (!GameState.instance.isPaused)
        {
            GetComponent<FirstPersonController>().enabled = true;
        }
        
    }

    public void fullHealth()
    {
        health = startHealth;
        healthSlider.value = health;
    }

    
}
