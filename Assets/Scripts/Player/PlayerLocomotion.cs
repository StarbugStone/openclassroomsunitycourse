﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerLocomotion : MonoBehaviour {

    public float fallTime = 1.5f;
    public float fatalFallTime = 3f;

    public AudioClip jump1;
    public AudioClip jump2;

    private CharacterController CharacterController;
    public Animator playerAnim;
    private float lastGrounded;

    private float horizontal;
    private float vertical;

    // Use this for initialization
    void Start () {
        CharacterController = GetComponent<CharacterController>();
        //playerAnim = GetComponentInChildren<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        playerAnim.SetBool("isGrounded", CharacterController.isGrounded);

        //running, direction and speed
        //playerAnim.SetBool("Running", Input.GetKey(KeyCode.LeftShift));

        Vector3 horizontalVelocity = CharacterController.velocity;
        horizontalVelocity = new Vector3(CharacterController.velocity.x, 0f, CharacterController.velocity.z);
        float horizontalSpeed = horizontalVelocity.magnitude;
        playerAnim.SetFloat("HorizontalSpeed", horizontalSpeed);

        //hor >0 = right
        //ver >0 = forward
        horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        vertical = CrossPlatformInputManager.GetAxis("Vertical");
        playerAnim.SetFloat("Vertical", vertical);
        playerAnim.SetFloat("Horizontal", horizontal);



        if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            playerAnim.SetTrigger("Jump");
            //play a jump sound if grounded
            //if(Random.Range(0,3) <1 && CharacterController.isGrounded)
            if (CharacterController.isGrounded)
            {
                SoundController.instance.RandomizeSfx(jump1, jump2);
            }
                
        }


        if (CharacterController.isGrounded)
        {
            lastGrounded = 0f;
        }
        else
        {
            lastGrounded += Time.deltaTime;
        }
        if (lastGrounded > fallTime)
        {
            if (lastGrounded > fatalFallTime)
            {
                //Debug.Log("fatal Fall Animation");
                //play the flat fall anim and apply fall damage
            }
            playerAnim.SetTrigger("Falling");
        }
    }
}
