﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvershootTrigger : MonoBehaviour {

    public AudioClip HomeRunSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && HomeRunSound != null)
        {
            SoundController.instance.PlaySingle(HomeRunSound);
        }
    }
}
